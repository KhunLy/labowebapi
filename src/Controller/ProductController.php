<?php


namespace App\Controller;


use App\Entity\Product;
use App\Form\ProductType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(path="API/product")
     * @Rest\View()
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Product::class);
        return $repo->findAll();
    }

    /**
     * @Rest\Delete(path="test")
     */
    public function test()
    {

    }

    /**
     * @Rest\Post(path="API/product")
     * @Rest\View()
     */
    public function add(Request $request)
    {
        $p = new Product();
        $form = $this->createForm(ProductType::class, $p);
        $data = json_decode($request->getContent(), true);
        $form->submit($data);
        $em = $this->getDoctrine()->getManager();
        $em->persist($p);
        $em->flush();
    }

    /**
     * @Rest\Post(path="API/basket/{productId}")
     * @Rest\View()
     */
    public function addToBasket($productId)
    {
        $user = $this->getUser();
        $repo = $this->getDoctrine()
            ->getRepository(Product::class);
        $produitAAjouter = $repo->find($productId);
        $user->addProduct($produitAAjouter);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
    }

    /**
     * @Rest\Get(path="API/basket")
     * @Rest\View()
     */
    public function userBasket()
    {
        $user = $this->getUser();
        return $user->getProducts();
    }

    /**
     * @Rest\Delete(path="API/basket/{productId}")
     * @Rest\View()
     */
    public function removeFromBasket($productId)
    {
        $user = $this->getUser();
        $repo = $this->getDoctrine()
            ->getRepository(Product::class);
        $produitARetirer = $repo->find($productId);
        $user->removeProduct($produitARetirer);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
    }
}