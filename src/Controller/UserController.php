<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\UserType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use mysql_xdevapi\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractFOSRestController
{
    /**
     * @Rest\Post(path="/API/register")
     * @Rest\View()
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        try{
            $u = new User();
            $form = $this->createForm(UserType::class, $u);
            $data = json_decode($request->getContent(), true);
            $form->submit($data);
            $encodedPwd = $encoder->encodePassword($u, $u->getPassword());
            $u->setPassword($encodedPwd);
            $em = $this->getDoctrine()->getManager();
            $em->persist($u);
            $em->flush();
            return $u;
        }catch (\Exception $e){
            return $e;
        }

    }
}